package com.sda.dp.abstractFactory.ex3;

public enum BIKE_TYPE {
    BICYCLE, TANDEM
}
