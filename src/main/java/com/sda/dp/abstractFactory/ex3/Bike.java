package com.sda.dp.abstractFactory.ex3;

public class Bike {
    private String brand;
    private int drivesQuantity, seats;
    private BIKE_TYPE type;

    public Bike(String brand, int drivesQuantity, int seats, BIKE_TYPE type) {
        this.brand = brand;
        this.drivesQuantity = drivesQuantity;
        this.seats = seats;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "brand='" + brand + '\'' +
                ", drivesQuantity=" + drivesQuantity +
                ", seats=" + seats +
                ", type=" + type +
                '}';
    }
}
//
//
//    Należy stworzyć aplikację symulującą fabrykę rowerów. Klasa Bike: ma markę, ilość miejsc,
//        ilość przerzutek, oraz typ rowera (bicycle, tandem).
//        BIKE_TYPE: BICYCLE, TANDEM
//        Stwórz i przetestuj fabrykę abstrakcyjną (BikeFactory) która pozwala na tworzenie:
//        Rowerów jednoosbowych, marki KROSS, które mają 5 przerzutek
//        Rowerów jednoosbowych, marki MERIDA, które mają 6 przerzutek
//        Tandemów, marki INIANA, które mają 3 przerzutki
//        Rowerów jednoosbowych, marki FELT, które mają 6 przerzutek
//        Tandemów, marki GOETZE, które mają jedną przerzutkę
