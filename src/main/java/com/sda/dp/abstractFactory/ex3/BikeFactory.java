package com.sda.dp.abstractFactory.ex3;

public abstract class BikeFactory {

    public static Bike createBikeKROSS(){
        return new Bike("Kross",5,1,BIKE_TYPE.BICYCLE);
    }

    public static Bike createBikeMerida(){
        return new Bike("Merida",6,1,BIKE_TYPE.BICYCLE);
    }

    public static Bike createBikeIniana(){
        return new Bike("Iniana", 3, 2,BIKE_TYPE.TANDEM);
    }
}