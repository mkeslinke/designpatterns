package com.sda.dp.abstractFactory.ex3;

public class Main {
    public static void main(String[] args) {
        Bike bike = BikeFactory.createBikeIniana();
        Bike bike1 = BikeFactory.createBikeKROSS();
        Bike bike2 = BikeFactory.createBikeMerida();

        System.out.println(bike);
        System.out.println(bike1);
        System.out.println(bike2);
    }
}
