package com.sda.dp.abstractFactory.ex2;

public class HPPC extends AbstractPC{

    public HPPC(String computerName, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(computerName, brand, cpu_power, gpu_power, isOverclocked);

        System.out.println("Utworzono " + this.computerName);
    }

    public static AbstractPC creatHPSUPER(){
        return new HPPC("HPSUPER", COMPUTER_BRAND.HP, 1000, 512, true);
    }

    public static AbstractPC createHPstandard(){
        return new HPPC("HP Standard", COMPUTER_BRAND.HP, 500, 128,false);

    }

}
