package com.sda.dp.abstractFactory.ex2;

public class AsusPC extends AbstractPC {
    public AsusPC(String computerName, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(computerName, brand, cpu_power, gpu_power, isOverclocked);

        System.out.println("Stworzono:  " + this.computerName);
    }

    public static AbstractPC createASUS1000() {
        return new AsusPC("ASUS 1000", COMPUTER_BRAND.ASUS, 1000, 128,false);
    }


    public static AbstractPC createASUS2000() {
        return new AsusPC("ASUS 2000", COMPUTER_BRAND.ASUS, 2000, 128, false);
    }
}
