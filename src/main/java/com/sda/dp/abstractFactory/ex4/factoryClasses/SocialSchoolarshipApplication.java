package com.sda.dp.abstractFactory.ex4.factoryClasses;

import java.util.List;

public class SocialSchoolarshipApplication {

private List<Double> grades;
private double totalFamilyIncome;

    public SocialSchoolarshipApplication(List<Double> grades, double totalFamilyIncome) {
        this.grades = grades;
        this.totalFamilyIncome = totalFamilyIncome;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public double getTotalFamilyIncome() {
        return totalFamilyIncome;
    }

    public void setTotalFamilyIncome(double totalFamilyIncome) {
        this.totalFamilyIncome = totalFamilyIncome;
    }

    @Override
    public String toString() {
        return "SocialSchoolarshipApplication{" +
                "grades=" + grades +
                ", totalFamilyIncome=" + totalFamilyIncome +
                '}';
    }
}
