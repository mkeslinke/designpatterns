package com.sda.dp.abstractFactory.ex4;

import com.sda.dp.abstractFactory.ex4.factoryClasses.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        Application app = new ApplicationFactory();
        List<Double> grade = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String reason = "";


        ApplicationGrades applicationGrades = ApplicationFactory.getGrades(scanner, grade);
        System.out.println(grade);


        ConditionalStayApplication application = ApplicationFactory.createConditionalStayApp(scanner, grade);
        System.out.println(application);

        SchoolarshipApplication application1 = ApplicationFactory.createSchoolarshipApp(scanner,grade);
        System.out.println(application1);

        SocialSchoolarshipApplication application2 = ApplicationFactory.createSocialApp(scanner, grade);
        System.out.println(application2);

        SemesterExtendApplication application3 = ApplicationFactory.createSemestrExtendApp(scanner);
        System.out.println(application3);

    }
}
