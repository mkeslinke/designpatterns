package com.sda.dp.abstractFactory.ex4.factoryClasses;

import java.util.List;

public class ApplicationGrades {

    private List<Double> grades;

    public ApplicationGrades(List<Double> grades) {
        this.grades = grades;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "ApplicationGrades{" +
                "grades=" + grades +
                '}';
    }
}
