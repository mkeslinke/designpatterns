package com.sda.dp.abstractFactory.ex4.factoryClasses;

import java.util.List;
import java.util.Scanner;

public class ConditionalStayApplication{
    protected List<Double> grade;
    protected String reason;


    public ConditionalStayApplication(List<Double> grade, String reason) {

        this.grade = grade;

        this.reason = reason;
    }

    public List<Double> getGrade() {
        return grade;
    }

    public void setGrade(List<Double> grade) {
        this.grade = grade;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ConditionalStayApplication{" +
                "grade=" + grade +
                ", reason='" + reason + '\'' +
                '}';
    }
}