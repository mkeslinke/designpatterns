package com.sda.dp.abstractFactory.ex4.factoryClasses;

public class SemesterExtendApplication {
    private String reason;

    public SemesterExtendApplication(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "SemesterExtendApplication{" +
                "reason='" + reason + '\'' +
                '}';
    }
}
