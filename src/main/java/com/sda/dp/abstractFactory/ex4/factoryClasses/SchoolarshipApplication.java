package com.sda.dp.abstractFactory.ex4.factoryClasses;

import java.util.List;

public class SchoolarshipApplication {

protected List<Double> grades;
protected List<String> extracurricularActivities;

    public SchoolarshipApplication(List<Double> grades, List<String> extracurricularActivities) {
        this.grades = grades;
        this.extracurricularActivities = extracurricularActivities;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public List<String> getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public void setExtracurricularActivities(List<String> extracurricularActivities) {
        this.extracurricularActivities = extracurricularActivities;
    }

    @Override
    public String toString() {
        return "SchoolarshipApplication{" +
                "grades=" + grades +
                ", extracurricularActivities=" + extracurricularActivities +
                '}';
    }
}


