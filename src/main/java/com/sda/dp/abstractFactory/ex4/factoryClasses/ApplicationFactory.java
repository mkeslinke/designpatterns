package com.sda.dp.abstractFactory.ex4.factoryClasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class ApplicationFactory {

    public static ApplicationGrades getGrades(Scanner scanner, List<Double> grade) {
        System.out.println("Proszę podać oceny");
        while (scanner.hasNextDouble()) {
            double grades = scanner.nextDouble();
            if (grades > 6) {
                System.out.println("Ocena zbyt wysoka, proszę podać jeszcze raz");
            } else if (scanner.equals("exit")) {
                break;
            } else {
                grade.add(grades);
            }
        }
        System.out.println("Twoje oceny to" + grade);
        return new ApplicationGrades(grade);
    }

    public static ConditionalStayApplication createConditionalStayApp(Scanner scanner, List<Double> grade) {
        System.out.println("Conditional Stay Application");

        System.out.println("Prosże podać powód");
        String reason = scanner.nextLine();
//        if (scanner.hasNextLine()){
//        String reason = scanner.nextLine();


        return new ConditionalStayApplication(grade, reason);
    }
//        return null;
//    }

    public static SchoolarshipApplication createSchoolarshipApp(Scanner scanner, List<Double> grade) {

        List<String> extraActivities = new ArrayList<>();

        System.out.println("Schoolarship Application");
        System.out.println("Proszę podać dodatkowe zajęcia (cyfra dla wyjścia)");
        while (!scanner.hasNextDouble()) {
            String activity = scanner.nextLine();
            extraActivities.add(activity);
        }

        return new SchoolarshipApplication(grade, extraActivities);
    }

    public static SocialSchoolarshipApplication createSocialApp(Scanner scanner, List<Double> grade) {

        System.out.println("Social Schoollarship Application");
        System.out.println("Proszę podać dochód dla gospodarstwa domowego");
        if (scanner.hasNextDouble()) {
            Double famIncome = scanner.nextDouble();

            return new SocialSchoolarshipApplication(grade, famIncome);
        }
        return null;
    }

    public static SemesterExtendApplication createSemestrExtendApp(Scanner scanner) {

        System.out.println("Semestr Extend Application");
        System.out.println("Proszę podać powód przedłużenia semsetru");
        if (scanner.hasNextLine()) {
            String reasonExtend = scanner.nextLine();
        return new SemesterExtendApplication(reasonExtend);
    }
        return null;
    }
}
