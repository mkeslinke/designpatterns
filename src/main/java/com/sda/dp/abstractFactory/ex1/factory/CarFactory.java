package com.sda.dp.abstractFactory.ex1.factory;



    public abstract class CarFactory {

        public static Car createBMW16(){
            return new Car("BMW", 30.0, 200, 4);
        }
    }
