package com.sda.dp.abstractFactory.ex1.factory;

public interface ICar {
    void drive();
    int getSpeed();
    double getGasLevel();
    double getHorsepower();
}
