package com.sda.dp.observer.ex2;

public class Main {
    public static void main(String[] args) {

        ChatRoom chatRoom = new ChatRoom("Pokoj");

        chatRoom.userLogin("jacek");
        chatRoom.userLogin("jadasdask");
        chatRoom.userLogin("jmarek");
        chatRoom.userLogin("ja2323");
        chatRoom.userLogin("kokod");
        chatRoom.userLogin("admin");

        chatRoom.sendMessage(5,"dzien dobry");


    }

}
