package com.sda.dp.observer.ex2;

public class Message {
    private String message;
    private int userID;

    public Message(int userID, String message) {
        this.message = message;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", userID=" + userID +
                '}';
    }
}