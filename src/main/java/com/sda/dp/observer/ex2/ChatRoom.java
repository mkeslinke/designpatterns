package com.sda.dp.observer.ex2;

import java.util.*;

public class ChatRoom extends Observable {
    private int userCounter = 0;
    private Map<Integer, ChatUser> userMap = new HashMap<>();
    private String roomName;
    private List<String> adminNicks = new ArrayList<>(Arrays.asList("admin"));

    public ChatRoom(String roomName) {
        this.roomName = roomName;
    }


    public void userLogin(String nick) {
        ChatUser newUser = new ChatUser(userCounter++, nick);
        for (ChatUser user : userMap.values()) {

            if (user.getNick().equals(newUser.getNick())) {
                System.out.println(" User już istnieje");
                return;
            }
        }
        if (adminNicks.contains(newUser.getNick())) {
            newUser.setAdmin(true);
        }
        userMap.put(newUser.getId(), newUser);
        addObserver(newUser);
    }

    public void sendMessage(int senderId, String message) {
        Message msg = new Message(senderId, message);

        setChanged();
        notifyObservers(new Message(senderId, message));
    }

    public void kickUser() {

    }
}
