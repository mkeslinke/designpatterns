package com.sda.dp.observer.example.example;
public class Main {
    public static void main(String[] args) {
        Restaurant r = new Restaurant();

        r.addEmployee("A");
        r.addEmployee("B");
        r.addEmployee("C");
        r.addEmployee("D");
        r.addEmployee("E");
        r.addEmployee("F");

        r.newOrder();
    }
}