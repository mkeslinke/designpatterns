package com.sda.dp.observer.example.example;

import java.util.LinkedList;
import java.util.List;

public class Restaurant {
    private List<IObserver> employees = new LinkedList<>();

    public void addEmployee(String name) {
        employees.add(new Employee(name));
    }

    public void addBoss(String name){
        employees.add(new Boss());
    }

    public void newOrder() {
        // powiadom pracownikow
        for (IObserver employee : employees) {
            employee.update("Komunikat");
        }
    }

    public void newOrder(String what) {
        Order zamowienie = new Order(what);

        for (IObserver employee : employees) {
            employee.update(zamowienie);
        }
    }
}