package com.sda.dp.observer.example.example;

public class Order {
    private String what;

    public Order(String what) {
        this.what = what;
    }

    @Override
    public String toString() {
        return "Order{" +
                "what='" + what + '\'' +
                '}';
    }
}