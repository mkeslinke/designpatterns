package com.sda.dp.observer.example.example;

public class Employee implements IObserver {
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public void update() {
        // pracownik został powiadomiony
    }

    @Override
    public void update(Object message) {
        if (message instanceof Order) {
            System.out.println("Nowe zamowienie");

        } else if (message instanceof String) {
            System.out.println("Nowa wiadomość");
        }
        System.out.println(name + " zostaje powiadomiony o " + message);
    }
}