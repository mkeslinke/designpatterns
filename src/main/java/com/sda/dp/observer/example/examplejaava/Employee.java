package com.sda.dp.observer.example.examplejaava;

import java.util.Observable;
import java.util.Observer;

public class Employee implements Observer {
    String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Order) {
            Order order = (Order) arg;
            System.out.println(name + " zostaje powiadomiony o " + order);
        } else if (arg instanceof String) {
            //...
        }

        if(o instanceof Restaurant){
            Restaurant restaurant = (Restaurant) o;
            restaurant.confirmMessage();
            // ...
        }
    }
}