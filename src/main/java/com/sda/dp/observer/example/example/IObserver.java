package com.sda.dp.observer.example.example;


/**
 * Created by amen on 12/3/17.
 */
public interface IObserver {
    public void update();
    public void update(Object message);
}