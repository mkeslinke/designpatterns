package com.sda.dp.observer.example.examplejaava;

import java.util.Observable;

public class Restaurant extends Observable {

    public void newOrder() {
        setChanged();
        notifyObservers("Komunikat");
    }

    public void newOrder(String ser) {
        Order o = new Order(ser);

        setChanged();
        notifyObservers(o);
    }

    public void confirmMessage(){
        System.out.println("Received confirmation");
    }
}