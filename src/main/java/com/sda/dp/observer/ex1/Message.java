package com.sda.dp.observer.ex1;

public class Message {
    private String numer, tresc;

    public Message(String numer, String tresc) {
        this.numer = numer;
        this.tresc = tresc;
    }

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    @Override
    public String toString() {
        return "Message{" +
                "numer='" + numer + '\'' +
                ", tresc='" + tresc + '\'' +
                '}';
    }
}
