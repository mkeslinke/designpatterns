package com.sda.dp.observer.ex1;

import javafx.collections.ObservableArray;

import java.util.Observable;

public class SMSStation extends Observable {

    public void newSMS() {
        setChanged();
        notifyObservers();
    }

    public void addPhone(String numer) {
        Phone phone = new Phone(numer);
        addObserver(phone);
    }

    public void sendSms(String numer, String tresc) {
        Message msg = new Message(numer, tresc);

        setChanged();
        notifyObservers(msg);
    }

    public void confirmSMS() {
        System.out.println("sms received");


    }
}