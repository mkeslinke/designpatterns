package com.sda.dp.bridge.ex1;

public class MyPrinter implements IPrinter{
    @Override
    public void printMessage(String string) {
        System.out.println(string);
    }
}
