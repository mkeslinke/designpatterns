package com.sda.dp.bridge.ex1;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class MyFilePrinter implements IPrinter {


    @Override
    public void printMessage(String string) throws FileNotFoundException{
        PrintWriter printWriter = new PrintWriter("printer");
        printWriter.println(string);
        printWriter.close();
    }


}
