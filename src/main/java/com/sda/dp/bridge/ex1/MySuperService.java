package com.sda.dp.bridge.ex1;

import java.io.FileNotFoundException;

public class MySuperService {
    private IPrinter iPrinter;


    public MySuperService(IPrinter iPrinter) {
        this.iPrinter = iPrinter;
    }

    public void printData(String string) throws FileNotFoundException {
        iPrinter.printMessage(string);
    }
}
