package com.sda.dp.bridge.ex1;

import java.io.FileNotFoundException;

public interface IPrinter {
    void printMessage(String string) throws FileNotFoundException;


}
