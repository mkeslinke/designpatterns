package com.sda.dp.bridge.ex2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class VehicleConrollerMain {


    public static void main(String[] args) {
        List<IVehicle> vehicles = Arrays.asList(
                new Car("samochód"),
                new Boat("łódka"),
                new Aircraft("samolot"));

        Scanner scanner = new Scanner(System.in);
        System.out.println("1 - ruch w gore / 2 - ruch w dol / 3 - ruch w lewo / 4 - ruch w prawo");
        while (true) {
            int input = scanner.nextInt();
            if (input == 1) {
                vehicles.forEach(v -> v.moveUp());
            } else if (input == 2) {
                vehicles.forEach(v -> v.moveDown());
            } else if (input == 3) {
                vehicles.forEach(v -> v.moveLeft());
            } else if (input == 4) {
                vehicles.forEach(IVehicle::moveRight);
            }else {
                break;
            }
        }
    }
}
