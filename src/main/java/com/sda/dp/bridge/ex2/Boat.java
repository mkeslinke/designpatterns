package com.sda.dp.bridge.ex2;

public class Boat implements IVehicle{
private String name;

    public Boat(String name) {
        this.name = name;
    }


    @Override
    public void moveRight() {
        System.out.println("ruch w prawo");

    }

    @Override
    public void moveLeft() {
        System.out.println("ruch w lewo");
    }

    @Override
    public void moveUp() {
        System.out.println("ruch w gore");
    }

    @Override
    public void moveDown() {
        System.out.println("ruch w dol");
    }

    @Override
    public String toString() {
        return "Boat{" +
                "name='" + name + '\'' +
                '}';
    }
}
