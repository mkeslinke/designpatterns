package com.sda.dp.decorator.ex1;

public class TunedCar implements ICar {
    private ICar carToBeTuned;
    private boolean extraCharger = false;
    private boolean extraEngine = false;
    private boolean extraSeats = false;

    public TunedCar(ICar carToBeTuned, boolean extraCharger, boolean extraEngine, boolean extraSeats) {
        this.carToBeTuned = carToBeTuned;

        if (carToBeTuned.isHasCharger())
            this.extraCharger=false;
        else
        this.extraCharger = extraCharger;
        this.extraEngine = extraEngine;
        this.extraSeats = extraSeats;
    }

    @Override
    public double getHorsePower() {
        return 0;
    }

    @Override
    public boolean isHasCharger() {
        return false;
    }

    @Override
    public double getEngineCapacity() {
        return 0;
    }

    @Override
    public double getChargerPressure() {
        return 0;
    }

    @Override
    public ICar getCar() {
        return null;
    }

    @Override
    public String toString() {
        return "TunedCar{" +
                "carToBeTuned=" + carToBeTuned +
                ", extraCharger=" + extraCharger +
                ", extraEngine=" + extraEngine +
                ", extraSeats=" + extraSeats +
                '}';
    }

    public static class Builder{


        private ICar carToBeTuned;
        private boolean extraCharger;
        private boolean extraEngine;
        private boolean extraSeats;

        public Builder setCarToBeTuned(ICar carToBeTuned) {
            this.carToBeTuned = carToBeTuned;
            return this;
        }

        public Builder setExtraCharger(boolean extraCharger) {
            this.extraCharger = extraCharger;
            return this;
        }

        public Builder setExtraEngine(boolean extraEngine) {
            this.extraEngine = extraEngine;
            return this;
        }

        public Builder setExtraSeats(boolean extraSeats) {
            this.extraSeats = extraSeats;
            return this;
        }

        public TunedCar createTunedCar() {
            return new TunedCar(carToBeTuned, extraCharger, extraEngine, extraSeats);
        }
    }
}
