package com.sda.dp.decorator.ex1;

public class CarFactory extends Car{


    public CarFactory(double chargerPressure, double engineCapacity, int seatsNumber, boolean hasCharger, double horsePower) {
        super(chargerPressure, engineCapacity, seatsNumber, hasCharger, horsePower);
    }

    public static ICar creatLowClassCar(){
        return new Car(500,1600,5,false,130);
    }

    public static ICar createMidClassCar(){
        return new Car(750, 2200,5,true,178);
    }

    public static ICar createHighClassCar(){
        return new Car(1000,4200,4,true,290);
    }

}
