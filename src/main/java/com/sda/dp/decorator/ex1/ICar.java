package com.sda.dp.decorator.ex1;

public interface ICar {
    double getHorsePower();
    boolean isHasCharger();
    double getEngineCapacity();
    double getChargerPressure();
    ICar getCar();

}