package com.sda.dp.decorator.ex1;

public class Main {
    public static void main(String[] args) {
        ICar car = CarFactory.createMidClassCar();
        CarShop carShop = new CarShop();
        TunedCar tunedCar = (TunedCar) carShop.addExtraCharger(car);
        ICar car1 = CarFactory.creatLowClassCar();
        TunedCar tunedCar1 = (TunedCar) carShop.addExtraCharger(car1);

        System.out.println(car);
        System.out.println(tunedCar);

        System.out.println(car1);
        System.out.println(tunedCar1);
    }
}
