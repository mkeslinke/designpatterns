package com.sda.dp.decorator.ex1;

public class Car implements ICar{
    private final double chargerPressure;
    private final double engineCapacity;
    private final int seatsNumber;
    private final boolean hasCharger;
    private final double horsePower;

    public Car(double chargerPressure, double engineCapacity, int seatsNumber, boolean hasCharger, double horsePower) {

        this.chargerPressure = chargerPressure;
        this.engineCapacity = engineCapacity;
        this.seatsNumber = seatsNumber;
        this.hasCharger = hasCharger;
        this.horsePower = horsePower;
    }

    @Override
    public double getChargerPressure() {
        return chargerPressure;
    }

    @Override
    public ICar getCar() {
        return this;
    }

    @Override
    public double getEngineCapacity() {
        return engineCapacity;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public boolean isHasCharger() {
        return hasCharger;
    }

    @Override
    public double getHorsePower() {
        return horsePower;
    }

    @Override
    public String toString() {
        return "Car{" +
                "chargerPressure=" + chargerPressure +
                ", engineCapacity=" + engineCapacity +
                ", seatsNumber=" + seatsNumber +
                ", hasCharger=" + hasCharger +
                ", horsePower=" + horsePower +
                '}';
    }
}
