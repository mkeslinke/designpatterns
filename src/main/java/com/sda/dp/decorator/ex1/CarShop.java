package com.sda.dp.decorator.ex1;

public class CarShop {

    public ICar addExtraCharger(ICar car) {
        boolean charger = true;
        if (car.isHasCharger()) {
            charger = false;
        }
        return new TunedCar.Builder().setCarToBeTuned(car).setExtraCharger(true).setExtraEngine(true).setExtraSeats(false).createTunedCar();
    }

    public ICar addExtraEngine(ICar car) {
        return new TunedCar.Builder().setCarToBeTuned(car).setExtraCharger(false).setExtraEngine(true).setExtraSeats(false).createTunedCar();
    }


    public ICar addSeats(ICar car) {
        return new TunedCar.Builder().setCarToBeTuned(car).setExtraCharger(false).setExtraEngine(false).setExtraSeats(true).createTunedCar();

    }

}
