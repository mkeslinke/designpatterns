package com.sda.dp.threadregex.ex1;

public class Message implements Runnable{
    private String numer, tresc;

    public Message(String numer, String tresc) {
        this.numer = numer;
        this.tresc = tresc;
    }

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    @Override
    public String toString() {
        return "Message{" +
                "numer='" + numer + '\'' +
                ", tresc='" + tresc + '\'' +
                '}';
    }

    @Override
    public void run() {
        int tresc2= tresc.length();
        try{
            Thread.sleep(tresc2*100);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("WYsłano wiadomość" + tresc);
    }
}
