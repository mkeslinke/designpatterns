package com.sda.dp.threadregex.ex1;

import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {
    private String numer;

    public Phone(String numer) {
        this.numer = numer;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            Message msg = (Message) arg;
            if (msg.getNumer().equals(numer)) {
                System.out.println(numer + " otrzynal wiadomosc :" + msg);
            }
            if (o instanceof SMSStation) {
                SMSStation smsStation = (SMSStation) o;
                smsStation.confirmSMS();

            }
        }
    }
}
