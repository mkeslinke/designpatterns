package com.sda.dp.threadregex.ex1;

import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SMSStation extends Observable {

    public void newSMS() {
        setChanged();
        notifyObservers();
    }

    public void addPhone(String numer) {
        Phone phone = new Phone(numer);
        addObserver(phone);
    }

    public void sendSms(String numer, String tresc) {
        Message msg = new Message(numer, tresc);
        service.submit(msg);

        setChanged();
        notifyObservers(msg);
    }

    public void confirmSMS() {
        System.out.println("sms received");
    }
    private ExecutorService service = Executors.newFixedThreadPool(5);
}