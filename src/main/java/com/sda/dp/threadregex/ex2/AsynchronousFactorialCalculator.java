package com.sda.dp.threadregex.ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsynchronousFactorialCalculator {
    private ExecutorService service = Executors.newFixedThreadPool(2);
    private List<String> results = new ArrayList<>();



    public AsynchronousFactorialCalculator() {
    }
    public void calculate (int number){
        FactorialRequest request = new FactorialRequest(number);
        service.submit(request);
    }
}
