package com.sda.dp.threadregex.ex2;

public class FactorialRequest implements Runnable {
    private int number;

    public FactorialRequest(int i) {
        this.number = i;
    }

    @Override
    public void run() {
        int value = 1;
        for (int i = 1; i <= number; i++) {
            value *= i;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(value);
    }






}
