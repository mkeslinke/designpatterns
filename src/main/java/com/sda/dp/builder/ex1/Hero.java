package com.sda.dp.builder.ex1;

import javafx.util.Builder;

public class Hero {
    String name, surname, catsName, mothersName, fathersName;
    int weight, height, age;
    char classLetter;

    public Hero(String name, String surname, String catsName, String mothersName, String fathersName, int weight, int height, int age, char classLetter) {
        this.name = name;
        this.surname = surname;
        this.catsName = catsName;
        this.mothersName = mothersName;
        this.fathersName = fathersName;
        this.weight = weight;
        this.height = height;
        this.age = age;
        this.classLetter = classLetter;
    }


    public static class Builder{

        private String name;
        private String surname;
        private String catsName;
        private String mothersName;
        private String fathersName;
        private int weight;
        private int height;
        private int age;
        private char classLetter;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder setCatsName(String catsName) {
            this.catsName = catsName;
            return this;
        }

        public Builder setMothersName(String mothersName) {
            this.mothersName = mothersName;
            return this;
        }

        public Builder setFathersName(String fathersName) {
            this.fathersName = fathersName;
            return this;
        }

        public Builder setWeight(int weight) {
            this.weight = weight;
            return this;
        }

        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Builder setClassLetter(char classLetter) {
            this.classLetter = classLetter;
            return this;
        }

        public Hero createHero() {
            return new Hero(name, surname, catsName, mothersName, fathersName, weight, height, age, classLetter);
        }
    }
    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", catsName='" + catsName + '\'' +
                ", mothersName='" + mothersName + '\'' +
                ", fathersName='" + fathersName + '\'' +
                ", weight=" + weight +
                ", height=" + height +
                ", age=" + age +
                ", classLetter=" + classLetter +
                '}';
    }
}
