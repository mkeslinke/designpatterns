package com.sda.dp.builder.ex1;

public class Main {
    public static void main(String[] args) {
        Hero.Builder builder = new Hero.Builder();
        Hero character = builder.setAge(34).setCatsName("mrqeuczek").setFathersName("jan").createHero();

        Hero wacek = new Hero.Builder().setAge(23).setCatsName("jdsqweads").createHero();
}
}
