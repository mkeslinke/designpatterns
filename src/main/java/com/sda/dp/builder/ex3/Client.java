package com.sda.dp.builder.ex3;

import java.util.List;

public class Client {
    private String name;
    private List<Mail> mailBox;

    public Client(String name, List<Mail> mailBox) {
        this.name = name;
        this.mailBox = mailBox;
    }

    public void readMail(Mail m) {

    }
public static class Builder {
    private String name;
    private List<Mail> mailBox;

    public Builder setName(String name) {
        this.name = name;
        return this;
    }

    public Builder setMailBox(List<Mail> mailBox) {
        this.mailBox = mailBox;
        return this;
    }

    public Client createClient() {
        return new Client(name, mailBox);
    }
}
}


//
//
//
//    Stwórz klasę Client która:
//        - posiada pole name
//        - posiada pole List<Mail> - lista/skrzynka wiadomości klienta
//        - readMail(Mail m) - która powoduje dodanie wiadomości do skrzynki i wypisanie
//        komunikatu:
//        "Klient " + this.getName() + " otrzymal maila"
