package com.sda.dp.builder.ex3;

public class Mail {
    private String text, sender;
    private int dateSent, dateReceive, ipSent, ipReceive;
    private String serverName, mailName, comProtocol, msgType;
    private boolean isEncrypted, isSpam;

    public Mail(String text, String sender, int dateSent, int dateReceive, int ipSent, int ipReceive, String serverName,
                String mailName, String comProtocol, String msgType, boolean isEncrypted, boolean isSpam) {
        this.text = text;
        this.sender = sender;
        this.dateSent = dateSent;
        this.dateReceive = dateReceive;
        this.ipSent = ipSent;
        this.ipReceive = ipReceive;
        this.serverName = serverName;
        this.mailName = mailName;
        this.comProtocol = comProtocol;
        this.msgType = msgType;
        this.isEncrypted = isEncrypted;
        this.isSpam = isSpam;
    }

    public static class Builder{

        private String text;
        private String sender;
        private int dateSent;
        private int dateReceive;
        private int ipSent;
        private int ipReceive;
        private String serverName;
        private String mailName;
        private String comProtocol;
        private String msgType;
        private boolean isEncrypted;
        private boolean isSpam;

        public Builder setText(String text) {
            this.text = text;
            return this;
        }

        public Builder setSender(String sender) {
            this.sender = sender;
            return this;
        }

        public Builder setDateSent(int dateSent) {
            this.dateSent = dateSent;
            return this;
        }

        public Builder setDateReceive(int dateReceive) {
            this.dateReceive = dateReceive;
            return this;
        }

        public Builder setIpSent(int ipSent) {
            this.ipSent = ipSent;
            return this;
        }

        public Builder setIpReceive(int ipReceive) {
            this.ipReceive = ipReceive;
            return this;
        }

        public Builder setServerName(String serverName) {
            this.serverName = serverName;
            return this;
        }

        public Builder setMailName(String mailName) {
            this.mailName = mailName;
            return this;
        }

        public Builder setComProtocol(String comProtocol) {
            this.comProtocol = comProtocol;
            return this;
        }

        public Builder setMsgType(String msgType) {
            this.msgType = msgType;
            return this;
        }

        public Builder setIsEncrypted(boolean isEncrypted) {
            this.isEncrypted = isEncrypted;
            return this;
        }

        public Builder setIsSpam(boolean isSpam) {
            this.isSpam = isSpam;
            return this;
        }

        public Mail createMail() {
            return new Mail(text, sender, dateSent, dateReceive, ipSent, ipReceive, serverName, mailName, comProtocol, msgType, isEncrypted, isSpam);
        }
    }
}