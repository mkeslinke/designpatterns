package com.sda.dp.singleton.ex1;

/**
 * Created by amen on 12/2/17.
 */
public class HealthDepartment {
    private WaitingRoomMachine machine = new WaitingRoomMachine();
    private Reception cashRegistry = new Reception();

    public void generateTicketMachine() {
        System.out.println(machine.generateTicket());
    }

    public void generateTicketReception() {
        System.out.println(cashRegistry.generateTicket());
    }

}
