package com.sda.dp.singleton.ex1;

import java.util.Scanner;

/**
 * Created by amen on 12/2/17.
 */
public class Main {
    public static void main(String[] args) {
        // forma 4
//        TicketGenerator.INSTANCE.metoda();
        // forma 1
        TicketGenerator.getInstance().getCounter();

        Scanner sc = new Scanner(System.in);

        HealthDepartment healthDepartment = new HealthDepartment();

        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine().trim().toLowerCase();

            if (line.startsWith("quit")) {
                break;
            } else if (line.startsWith("machine")) {
                healthDepartment.generateTicketMachine();
            } else if (line.startsWith("reception")) {
                healthDepartment.generateTicketReception();
            }
        }
    }
}
