package com.sda.dp.singleton.ex1;

/**
 * Created by amen on 12/2/17.
 */
public class Reception {
    public Ticket generateTicket(){
        int ticketId = TicketGenerator.getInstance().getCounter();

        Ticket ticket = new Ticket("Reception", ticketId);
        return ticket;
    }
}
