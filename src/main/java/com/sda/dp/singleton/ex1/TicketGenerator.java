package com.sda.dp.singleton.ex1;

/**
 * Created by amen on 12/2/17.
 */
public /*enum*/ class TicketGenerator {
    /*INSTANCE;*/

    private int counter = 0;

    public int getCounter() {
        return counter++;
    }

    // forma 0
    /*
    public static TicketGenerator instance = new TicketGenerator();
    private TicketGenerator(){

    }
    */

    // forma 1

    private static TicketGenerator instance = new TicketGenerator();

    public static TicketGenerator getInstance(){
        return instance;
    }

    private TicketGenerator(){

    }

    // forma 2
    /*
    private static TicketGenerator instance;

    public static TicketGenerator getInstance() {
        if (instance == null) {
            instance = new TicketGenerator();
        }
        return instance;
    }


    private TicketGenerator() {

    }
    */


    // forma 3
    /*
    private static TicketGenerator instance;

    public static synchronized TicketGenerator getInstance() {
        if (instance == null) {
            synchronized (TicketGenerator.class) {
                if (instance == null) {
                    instance = new TicketGenerator();
                }
            }
        }
        return instance;
    }


    private TicketGenerator() {

    }
    */
}
