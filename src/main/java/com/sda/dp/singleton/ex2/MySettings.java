package com.sda.dp.singleton.ex2;

public enum MySettings {
    INSTANCE;

    private int zakresLiczby1;
    private int zakresLiczby2;
    private String dzialanie;
    private int iloscRund;

    public int getZakresLiczby1() {
        return zakresLiczby1;
    }

    public void setZakresLiczby1(int zakresLiczby1) {
        this.zakresLiczby1 = zakresLiczby1;
    }

    public int getZakresLiczby2() {
        return zakresLiczby2;
    }

    public void setZakresLiczby2(int zakresLiczby2) {
        this.zakresLiczby2 = zakresLiczby2;
    }

    public String getDzialanie() {
        return dzialanie;
    }

    public void setDzialanie(String dzialanie) {
        this.dzialanie = dzialanie;
    }

    public int getIloscRund() {
        return iloscRund;
    }

    public void setIloscRund(int iloscRund) {
        this.iloscRund = iloscRund;
    }
}
