package com.sda.dp.singleton.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class FileReader {
    private Scanner reader;


    public void readFile() {
        try {
            Scanner reader = new Scanner(new File("Config"));

            String[] zakresLiczby1tab = reader.nextLine().split("\\=");
            int zakresLiczby1 = Integer.parseInt(zakresLiczby1tab[1]);

            String[] zakresLiczby2tab = reader.nextLine().split("\\=");
            int zakresLiczby2 = Integer.parseInt(zakresLiczby2tab[1]);

            String[] dzialanieTab = reader.nextLine().split("\\=");
            String dzialanie = dzialanieTab[1];

            String[] iloscRundTab = reader.nextLine().split("\\=");
            int iloscRund = Integer.parseInt(iloscRundTab[1]);


            MySettings.INSTANCE.setZakresLiczby1(zakresLiczby1);
            MySettings.INSTANCE.setZakresLiczby2(zakresLiczby2);
            MySettings.INSTANCE.setDzialanie(dzialanie);
            MySettings.INSTANCE.setIloscRund(iloscRund);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }


//    String zakresLiczby2 = reader.nextInt();
//    String dzialanie = reader.nextLine();
//    int iloscRund = reader.nextInt();
    }

}
