package com.sda.dp.com.sda.dp.facade.ex1.frother;

import com.sda.dp.com.sda.dp.facade.ex1.ingredients.MilkIngredient;

public class MilkFrother {

    public FoamedMilkIngredient froth(MilkIngredient milkIngredient) throws MilkFrotherException {
        double random = java.lang.Math.random();
        if (random>0.8){
            throw new MilkFrotherException("Spieniacz spieniony");
        }else {
            System.out.println("Wszystko się ładnie pieni");
        }return new FoamedMilkIngredient();
    }

}
