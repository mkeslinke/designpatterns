package com.sda.dp.com.sda.dp.facade.ex1;

import com.sda.dp.com.sda.dp.facade.ex1.ingredients.CoffeIngredient;

import java.util.ArrayList;
import java.util.List;

public class CoffeeBeverage {
    private List<CoffeIngredient> coffeIngredientList = new ArrayList<CoffeIngredient>();

    public CoffeeBeverage(List<CoffeIngredient> coffeIngredientList) {
        this.coffeIngredientList = this.coffeIngredientList;
    }

    public CoffeeBeverage() {
    }

    public boolean addIngredient(CoffeIngredient coffeeIngredient) {
        return coffeIngredientList.add(coffeeIngredient);
    }

    public List<CoffeIngredient> getCoffeIngredientList() {
        return coffeIngredientList;
    }

    public void setCoffeIngredientList(List<CoffeIngredient> coffeIngredientList) {
        this.coffeIngredientList = coffeIngredientList;
    }

    @Override
    public String toString() {
        return "CoffeeBeverage{" +
                "coffeIngredientList=" + coffeIngredientList +
                '}';
    }
}
