package com.sda.dp.com.sda.dp.facade.ex1.frother;

public class MilkFrotherException extends Exception {
    public MilkFrotherException(String message) {
        super(message);
    }
}
