package com.sda.dp.com.sda.dp.facade.ex1;

import com.sda.dp.com.sda.dp.facade.ex1.ingredients.CoffeIngredient;

public class AdvancedCoffeMachine {
    private CoffeeBeverage currentCoffe = new CoffeeBeverage();

    public boolean addIngredient(CoffeIngredient coffeIngredient) {
        return currentCoffe.addIngredient(coffeIngredient);
    }

    public CoffeeBeverage makeCoffeBeverage() {
        CoffeeBeverage coffe = currentCoffe;
        currentCoffe = new CoffeeBeverage();
        return coffe;
    }

}

