package com.sda.dp.com.sda.dp.facade.ex1.frother;

import com.sda.dp.com.sda.dp.facade.ex1.ingredients.CoffeIngredient;

public class FoamedMilkIngredient extends CoffeIngredient {
    FoamedMilkIngredient() {
    }
}
