package com.sda.dp.com.sda.dp.facade.ex1.ingredients;

public class WaterIngredient extends CoffeBean {
    private double waterCapacityMM;

    public WaterIngredient(double waterCapacityMM) {
        this.waterCapacityMM = waterCapacityMM;
    }

    public double getWaterCapacityMM() {
        return waterCapacityMM;
    }

    public void setWaterCapacityMM(double waterCapacityMM) {
        this.waterCapacityMM = waterCapacityMM;
    }

    @Override
    public String toString() {
        return "WaterIngredient{" +
                "waterCapacityMM=" + waterCapacityMM +
                '}';
    }
}
