package com.sda.dp.com.sda.dp.facade.ex1;

import com.sda.dp.com.sda.dp.facade.ex1.frother.MilkFrother;
import com.sda.dp.com.sda.dp.facade.ex1.grinder.CoffeGrinder;
import com.sda.dp.com.sda.dp.facade.ex1.ingredients.CoffeBean;
import com.sda.dp.com.sda.dp.facade.ex1.ingredients.MilkIngredient;
import com.sda.dp.com.sda.dp.facade.ex1.ingredients.SugarIngredient;
import com.sda.dp.com.sda.dp.facade.ex1.ingredients.WaterIngredient;

public class Main {
    public static void main(String[] args) {
        CoffeGrinder coffeeGrinder = new CoffeGrinder();
        MilkFrother milkFrother = new MilkFrother();

        AdvancedCoffeMachine advancedCoffeMachine = new AdvancedCoffeMachine();
        CoffeBean coffeBean = new CoffeBean();
        MilkIngredient milkIngredient = new MilkIngredient();
        SugarIngredient sugarIngredient = new SugarIngredient();
        WaterIngredient waterIngredient = new WaterIngredient(30);

//
//        advancedCoffeMachine.addIngredient(coffeeGrinder.grind(coffeBean));
//        advancedCoffeMachine.addIngredient(waterIngredient);
//        System.out.println(advancedCoffeMachine.makeCoffeBeverage());

        SimpleCoffeMachine simpleCoffeMachine = new SimpleCoffeMachine();
        coffeeGrinder.empty();
        System.out.println(simpleCoffeMachine.makeEspresso());




    }
}

