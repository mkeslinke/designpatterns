package com.sda.dp.com.sda.dp.facade.ex1;

import com.sda.dp.com.sda.dp.facade.ex1.frother.MilkFrother;
import com.sda.dp.com.sda.dp.facade.ex1.frother.MilkFrotherException;
import com.sda.dp.com.sda.dp.facade.ex1.grinder.CoffeGrinder;
import com.sda.dp.com.sda.dp.facade.ex1.ingredients.CoffeBean;
import com.sda.dp.com.sda.dp.facade.ex1.ingredients.MilkIngredient;
import com.sda.dp.com.sda.dp.facade.ex1.ingredients.WaterIngredient;

public class SimpleCoffeMachine {
    CoffeGrinder coffeeGrinder = new CoffeGrinder();
    MilkFrother milkFrother = new MilkFrother();
    AdvancedCoffeMachine advancedCoffeMachine = new AdvancedCoffeMachine();

    public CoffeeBeverage makeEspresso(){
        WaterIngredient waterIngredient = new WaterIngredient(30);
        CoffeBean coffeBean = new CoffeBean();
        advancedCoffeMachine.addIngredient(coffeeGrinder.grind(coffeBean));
        advancedCoffeMachine.addIngredient(waterIngredient);
        return advancedCoffeMachine.makeCoffeBeverage();
    }

    public CoffeeBeverage makeLatte() throws MilkFrotherException {
        CoffeBean coffeBean = new CoffeBean();
        WaterIngredient waterIngredient = new WaterIngredient(100);
        MilkIngredient milkIngredient = new MilkIngredient();
        advancedCoffeMachine.addIngredient(coffeeGrinder.grind(coffeBean));
        advancedCoffeMachine.addIngredient(milkFrother.froth(milkIngredient));
        advancedCoffeMachine.addIngredient(waterIngredient);
        return advancedCoffeMachine.makeCoffeBeverage();
    }
}
