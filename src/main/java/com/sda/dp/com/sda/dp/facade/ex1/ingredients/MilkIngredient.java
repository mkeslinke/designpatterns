package com.sda.dp.com.sda.dp.facade.ex1.ingredients;

public class MilkIngredient extends CoffeIngredient {
    @Override
    public String toString() {
        return "MilkIngredient{}";
    }
}
