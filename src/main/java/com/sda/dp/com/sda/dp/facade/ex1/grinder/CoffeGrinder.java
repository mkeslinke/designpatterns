package com.sda.dp.com.sda.dp.facade.ex1.grinder;

import com.sda.dp.com.sda.dp.facade.ex1.ingredients.CoffeBean;

public class CoffeGrinder {

    private int wasteContainerCount;

    public CoffeGrinder(int wasteContainerCount) {
        this.wasteContainerCount = wasteContainerCount;
    }

    public CoffeGrinder() {

    }

    public void empty(){
        wasteContainerCount = 0;
    }

    public CoffePowderIngredient grind(CoffeBean coffeBean){
        if (wasteContainerCount>3) try {
            throw new CoffeGrinderException("W śmietniczku za dużo ziarenek, opróźnij");
        } catch (CoffeGrinderException e) {
            e.printStackTrace();
        }else {
            System.out.println("Śmietniczek sie zapełnia");

        wasteContainerCount++;}
        return new CoffePowderIngredient();
    }
}