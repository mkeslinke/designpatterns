package com.sda.dp.com.sda.dp.facade.ex1.grinder;

import com.sda.dp.com.sda.dp.facade.ex1.ingredients.CoffeIngredient;

public class CoffePowderIngredient extends CoffeIngredient {

     CoffePowderIngredient() {
    }

    @Override
    public String toString() {
        return "CoffePowderIngredient{}";
    }
}
