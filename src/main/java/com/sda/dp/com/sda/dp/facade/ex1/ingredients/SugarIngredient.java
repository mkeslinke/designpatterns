package com.sda.dp.com.sda.dp.facade.ex1.ingredients;

public class SugarIngredient extends CoffeBean {
    @Override
    public String toString() {
        return "SugarIngredient{}";
    }
}
