package com.sda.dp.strategy.ex1;

public class MediumSetting implements IGraphicSetting {
    @Override
    public void getNeededProccessingPower() {
        System.out.println("Proccessing Power needed : 750 MHz");
    }

    @Override
    public void processFrame(int[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                table[i][j] = 23;
                System.out.println(" ");
            }
        }
    }

    @Override
    public String toString() {
        return "MediumSetting{}";
    }
}

