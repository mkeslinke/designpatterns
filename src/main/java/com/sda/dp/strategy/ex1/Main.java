package com.sda.dp.strategy.ex1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        GraphicsCard graphicsCard = new GraphicsCard();

        Scanner scanner = new Scanner(System.in);
        int[][] ramka = new int[10][10];
        boolean working = true;

        while (working) {
            String line = scanner.nextLine();
            if (line.toLowerCase().trim().equals("low")) {
                graphicsCard.setStrategy(new LowSetting());
            } else if (line.toLowerCase().trim().equals("medium")) {
                graphicsCard.setStrategy(new MediumSetting());
            } else if (line.toLowerCase().trim().equals("high")) {
                graphicsCard.setStrategy(new HDSetting());
            }
            graphicsCard.getNeededProccessingPower();
            graphicsCard.processFrame(ramka);
            for (int i = 0; i < ramka.length; i++) {
                System.out.println(" ");
                for (int j = 0; j < ramka[i].length; j++) {
                    System.out.print(ramka[i][j]);


                }
                System.out.println(" ");
            }
            if (line.equals("exit")) {
                break;
            }
        }

    }
}
