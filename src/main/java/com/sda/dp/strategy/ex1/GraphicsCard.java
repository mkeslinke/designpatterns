package com.sda.dp.strategy.ex1;

public class GraphicsCard {
    private IGraphicSetting strategy = new HDSetting();
    private int[][] ramka;


    public void setStrategy(IGraphicSetting strategy) {
        System.out.println("Zmieniam ustawienia karty na " + strategy);
        this.strategy = strategy;

    }

    public void getNeededProccessingPower() {
        strategy.getNeededProccessingPower();
    }


    public void processFrame(int[][] table) {
        strategy.processFrame(table);
    }

}