package com.sda.dp.strategy.ex1;

public interface IGraphicSetting {

    void getNeededProccessingPower();

    void processFrame(int[][] ramka);


}


