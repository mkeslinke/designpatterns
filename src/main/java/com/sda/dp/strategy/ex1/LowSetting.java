package com.sda.dp.strategy.ex1;

public class LowSetting implements IGraphicSetting{
    @Override
    public void getNeededProccessingPower() {
        System.out.println("Proccessing power needed: 500MHz");
    }

    @Override
    public void processFrame(int[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                table[i][j] = 265;

            }
        }
    }

    @Override
    public String toString() {
        return "LowSetting{}";
    }
}
