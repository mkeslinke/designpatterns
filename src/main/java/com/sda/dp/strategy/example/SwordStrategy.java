package com.sda.dp.strategy.example;

public class SwordStrategy implements IStrategy {

    @Override
    public void fight() {
        System.out.println("Walczę mieczem!");
    }
}
