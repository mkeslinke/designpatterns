package com.sda.dp.strategy.example;

public class Hero {

    private IStrategy strategy = new BowStrategy();

    public void setStrategy(IStrategy strategy) {
        System.out.println("Zmieniam strategie: " + strategy);
        this.strategy = strategy;
    }

    public void fight() {
        strategy.fight();
    }


}
