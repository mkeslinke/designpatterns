package com.sda.dp.strategy.example;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Hero h = new Hero();

        Scanner sc = new Scanner(System.in);
        boolean working = true;
        while (working) {
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("fight")) {
                h.fight();
            } else if (line.equalsIgnoreCase("bow")) {
                h.setStrategy(new BowStrategy());
            } else if (line.equalsIgnoreCase("chair")) {
                h.setStrategy(new ChairStrategy());
            } else if (line.equalsIgnoreCase("sword")) {
                h.setStrategy(new SwordStrategy());
            }
        }
        h.fight();
    }
}
